# -*- coding: utf-8 -*-
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import os
import yaml

def kv(k, v):
    if type(v) is unicode:
        return '([{0}] {1} = {2})'.format(type(v), k, v.encode('utf-8'))
    else:
        return '([{0}] {1} = {2})'.format(type(v), k, v)

def read_config(config_file):
    if os.path.isfile(config_file):
        try:
            with open(config_file) as fh:
                config = yaml.safe_load(fh)
                return config
        except IOError as e:
            # TODO not here but in caller
            print("({})".format(e))
            raise
    else:
        return None

def save_config(config, config_file):
    try:
        with open(config_file, 'w') as fh:
            yaml.dump(config, fh)
    except IOError as e:
        # TODO not here but in caller
        print("({})".format(e))
        raise

if __name__ == '__main__':
    # read credentials from environment variables
    user = os.environ.get('HUUTONET_USER')
    passwd = os.environ.get('HUUTONET_PASSWD')

    if not user or not passwd:
        raise Exception('environment variables not set')

    # read config from file
    config_file = '.huutonet'
    config = read_config(config_file)
    if config is None:
        # default config
        config = { 'republish_time': datetime.now() }
    print 'republish_time = {0}'.format(config['republish_time'])

    # init webdrive
    driver = webdriver.Firefox()
    driver.maximize_window()

    # login
    driver.get('https://www.huuto.net/kirjaudu')
    e = driver.find_element_by_id('userid')
    e.send_keys(user)
    e = driver.find_element_by_id('password')
    e.send_keys(passwd)
    e.submit()

    # republish

    # TODO browse more than one page
    # TODO record republishing and filter out those already republished

    driver.get('https://www.huuto.net/omasivu/myynnit/unsold')

    sale_list = driver.find_element_by_id('saleList')

    items = sale_list.find_elements_by_class_name('item')

    i = 0
    for item in items:
        i = i + 1
        desc = item.find_element_by_class_name('spandesc')
        title = desc.find_element_by_class_name('title').text
        link = desc.find_element_by_class_name('title').get_attribute('href')
        id_ = link.split('/')[-1]
        exclude = False if ('exclude' not in config) else (int(id_) in config['exclude'])
        closed_at_str = desc.find_element_by_class_name('closingtime').text
        closed_at = datetime.strptime(closed_at_str, '%d.%m.%Y %H:%M')
        republish = (closed_at > config['republish_time']) and not exclude
        republish_link = None
    
        if republish:
            republish_link = 'https://www.huuto.net/uusi-ilmoitus/' + id_ + '/republish'

        print '----------'
        print '{0}: title: {1}'.format(i, title.encode('utf-8'))
        print '{0}: link: {1}'.format(i, link.encode('utf-8'))
        print '{0}: id: {1}'.format(i, id_)
        print '{0}: exclude: {1}'.format(i, exclude)
        print '{0}: closed_at: {1}'.format(i, closed_at)
        print '{0}: republish: {1}'.format(i, republish)
        print '{0}: republish_link: {1}'.format(i, republish_link)

        # TODO catch exceptions and re-try
        if republish:
            #
            # 1) open new tab
            #

            ActionChains(driver).key_down(Keys.CONTROL).send_keys('t').key_up(Keys.CONTROL).perform()
            driver.get(republish_link)

            #
            # 2) page 1/2
            #
            e = WebDriverWait(driver, 25).until(EC.presence_of_element_located((By.ID, 'continue')))
            e.submit()

            #
            # 3) page 2/2
            #

            # set aukioloaika to 14 days
            # input element is not visible, use li instead
            e = WebDriverWait(driver, 25).until(EC.visibility_of_element_located((By.XPATH, '//li[input[@id="open_days14"]]')))
            e.click()

            e = WebDriverWait(driver, 25).until(EC.visibility_of_element_located((By.XPATH, '//input[@id="preview"]')))
            e.click()

            #
            # 4) preview page
            # Firebug: $x('//a[text() = "Julkaise ilmoitus"]')
            #
            e = WebDriverWait(driver, 25).until(EC.presence_of_element_located((By.XPATH, '//a[text() = "Julkaise ilmoitus"]')))
            e.click()

            #
            # 5) final page
            # Firebug: $x('//div[@class = "alert-box success"]')

            # wait
            e = WebDriverWait(driver, 25).until(EC.presence_of_element_located((By.XPATH, '//div[@class = "alert-box success"]')))
            # close tab
            ActionChains(driver).key_down(Keys.CONTROL).send_keys('w').key_up(Keys.CONTROL).perform()
            # return to default content
            driver.switch_to_default_content()

    config['republish_time'] = datetime.now()
    save_config(config, config_file)
