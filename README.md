huutonet-tools
==============

A Selenium WebDrive program (Python) to republish unsold [huuto.net](http://www.huuto.net) items.

[huuto.net](http://www.huuto.net) is a popular Finnish on-line marketplace.

Everything runs on a [Vagrant](https://www.vagrantup.com) virtual machine: janihur/ubuntu-1604-lxde-desktop

Vagrant
-------

Install [VirtualBox](https://www.virtualbox.org) and [Vagrant](https://www.vagrantup.com).

```
vagrant up
```

* username: vagrant
* password: vagrant

republish.py
------------

Republish unsold [huuto.net](http://www.huuto.net) items. Tested on Python 2.7.6 only.

Requires environment variables:

* `HUUTONET_USER`
* `HUUTONET_PASSWD`

```
export HUUTONET_USER=my_username
export HUUTONET_PASSWD=my_password
```

Run:

```
python republish.py
```

Make sure the window where the browser runs is big enough so that the buttons that needs to be pressed are visible.

Configuration file `.huutonet`:

```
exclude: !!set {123456: null, 1234567: null}
republish_time: 2015-11-21 00:00:00
```

Selenium WebDrive Documents
---------------------------

* http://selenium-python.readthedocs.org/en/latest/index.html
* http://seleniumhq.github.io/selenium/docs/api/java/index.html
* http://seleniumhq.github.io/selenium/docs/api/java/org/openqa/selenium/WebDriver.html
* http://seleniumhq.github.io/selenium/docs/api/java/org/openqa/selenium/WebElement.html
